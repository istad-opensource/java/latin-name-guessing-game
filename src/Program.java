import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter your name -> ");
        String fullName = scanner.nextLine();

        System.out.println(fullName);

        int sum = 0;
        for (int i = 0; i < fullName.length(); i++) {
            //System.out.println(fullName.charAt(i));
            sum += switch (fullName.toLowerCase().charAt(i)) {
                case 'a', 'j', 's' -> 1;
                case 'b', 'k', 't' -> 2;
                case 'c', 'l', 'u' -> 3;
                case 'd', 'm', 'v' -> 4;
                case 'e', 'n', 'w' -> 5;
                case 'f', 'o', 'x' -> 6;
                case 'g', 'p', 'y' -> 7;
                case 'h', 'q', 'z' -> 8;
                case 'i', 'r' -> 9;
                default -> 0;
            };
        }

        System.out.println("First sum = " + sum);

        while (String.valueOf(sum).length() > 1) {
            //int remainder = sum % 10;
            //int result = sum / 10;
            sum = (sum % 10) + (sum / 10);
        }

        System.out.println(sum);

    }
}
