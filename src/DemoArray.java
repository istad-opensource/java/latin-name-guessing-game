public class DemoArray {

    public static void main(String[] args) {

        int[] numbers = new int[] {1,2,3}; // Declare and allocate array
        numbers[0] = 100;
        numbers[1] = 200;
        numbers[2] = 300;
        numbers[3] = 400;
        numbers[4] = 500;

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

        for (Integer number : numbers) {
            System.out.println(number);
        }

    }

}
